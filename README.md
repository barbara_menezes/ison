# ISON

<img src="/uploads/3fe188fa0f7f8239fad9860f101c64ce/436fe477-1c25-4d9b-a374-d549f2bf1cb1.jpg" alt="logo" width=200 height=200>

# DESCRIÇÃO DO PROJETO

<p>A solução consiste em um sistema web projetado para enviar notificações quando o sistema que opera em um servidor na nuvem ficar inativo. 
Desta forma, será necessário o cadastro dos dados do projeto e dos responsáveis que receberam o alerta. </p>
O processo precisa conter uma solução que contenha front-end, back-end e aplicativo.

# TECNOLOGIAS UTILIZADAS
#### FRONT-END
* HTML
* CSS
* BOOTSTRAP

#### BACK-END
* JAVA
* SPRINGBOOT

#### BANCO DE DADOS
* POSTEGRESQL

# AUTORES

* Bárbara Menezes
* Ian Merlo
* João Santos

# MATERIAIS DE REFERÊNCIA
 <p> 
 <a href="https://medium.com/@starsoftwares.corp/mepeando-e-populando-classes-com-springboot-hibernate-primefaces-e-postgresql-608a4038b62f"> MEDIUM SPRINGBOOT</a>
 </p>
 
 <a href="https://medium.com/@raullesteves/github-como-fazer-um-readme-md-bonitão-c85c8f154f8"> MARKDOWN </a>