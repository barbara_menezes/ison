package br.com.example.repository;

import org.springframework.stereotype.Repository;

import br.com.example.abstracts.BaseRepository;
import br.com.example.domain.Usuario;

@Repository
public interface UsuarioRepository extends BaseRepository<Usuario> {

}
