package br.com.example.exception;

import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;

public class TangerinoResponseException extends ServiceException {

    private static final long serialVersionUID = 1L;

    private String id;

    private String message;

    private HttpStatus status;

    public TangerinoResponseException(String id, String message, HttpStatus status) {
        super(message);
        this.id = id;
        this.message = message;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
