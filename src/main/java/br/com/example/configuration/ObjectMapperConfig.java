package br.com.example.configuration;

import javax.inject.Inject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@Configuration
public class ObjectMapperConfig {

    @Inject
    private ApplicationContext applicationContext;

    @Bean
    @Primary
    public ObjectMapper jsonObjectMapper() {
        Hibernate5Module hibernate5Module = new Hibernate5Module();
        hibernate5Module.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, false);
        hibernate5Module.configure(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION, false);
        Jackson2ObjectMapperBuilder mapperBuilder = Jackson2ObjectMapperBuilder.json();
        mapperBuilder.applicationContext(this.applicationContext);
        mapperBuilder.featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapperBuilder.modulesToInstall(hibernate5Module);
        return mapperBuilder.build();
    }
}