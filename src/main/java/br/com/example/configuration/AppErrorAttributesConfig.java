package br.com.example.configuration;

import java.util.Map;

import org.hibernate.service.spi.ServiceException;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;

@Configuration
public class AppErrorAttributesConfig {

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(RequestAttributes attributes, boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(attributes, includeStackTrace);
                Throwable error = getError(attributes);
                if (error instanceof ServiceException) {
                    errorAttributes.put("rootException", "ServiceException");
                    errorAttributes.put("message", error.getMessage());
                }
                return errorAttributes;
            }
        };
    }
}