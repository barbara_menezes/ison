package br.com.example.configuration;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import lombok.extern.java.Log;

@Log
@Component
public class CustomSecurityFilter extends GenericFilterBean {

    private List<String> publicPaths = Arrays.asList(
            "/v2/api-docs",
            "/favicon.ico",
            "/configuration/ui",
            "/swagger-resources",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/",
            "swagger.index",
            "swagger.json",
            "/health"
    );


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest httpRequest = asHttp(request);
//        if (!anyMatch(this.publicPaths, httpRequest.getServletPath())) {
//            String token = httpRequest.getHeader("Authorization");
//            if (StringUtils.isBlank(token)) {
//                throw new UnauthenticatedException();
//            }
//            log.info(token);
//        }
        chain.doFilter(request, response);
    }

    private HttpServletRequest asHttp(ServletRequest request) {
        return (HttpServletRequest) request;
    }

    private boolean anyMatch(List<String> paths, String fullPath) {
        return paths.stream().anyMatch(fullPath::contains);
    }
}
