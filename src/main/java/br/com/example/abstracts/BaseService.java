package br.com.example.abstracts;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public abstract class BaseService<E extends BaseEntity> {

    public abstract BaseRepository<E> getRepository();

    public E findOne(Long id) {
        return getRepository().findOne(id);
    }

    public List<E> findAll() {
        return getRepository().findAll();
    }

    public Page<E> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    public E insert(E entity) {
        entity.setDataInsercao(new Date());
        return getRepository().save(entity);
    }

    public E update(E entity) {
        entity.setDataAlteracao(new Date());
        return getRepository().save(entity);
    }

    public void flush() {
        getRepository().flush();
    }
    
    public void delete(Long id) {
    	getRepository().delete(id);
    }

}
