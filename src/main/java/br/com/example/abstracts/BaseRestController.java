package br.com.example.abstracts;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

public abstract class BaseRestController<E extends BaseEntity> {

    protected abstract BaseService<E> getService();

    
    @HystrixCommand
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lastUpdate", value = "purpose of this property is to return only data with update more recent than this date - timeinmilis ", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "Current Page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "Page Size", dataType = "int", paramType = "query")
    })
    @RequestMapping(method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public Page<E> findAll(
            @RequestParam(value = "lastUpdate", required = false) Long lastUpdate,
            Pageable pageable
    ) {
        return getService().findAll(pageable);
    }

    @HystrixCommand
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find one by id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path")
    })
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public E findOne(@PathVariable Long id) {
        return getService().findOne(id);
    }

    @HystrixCommand
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create new")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public E createNew(
            @RequestBody @Valid E dto
    ) {
        return getService().insert(dto);
    }

    @HystrixCommand
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update an existing")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    public E update(
            @RequestBody @Valid E dto
    ) {
        return getService().update(dto);
    }

    @HystrixCommand
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete one")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path")
    })
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    public void deleteOne(@PathVariable Long id) {
    	getService().delete(id);
    }
}
