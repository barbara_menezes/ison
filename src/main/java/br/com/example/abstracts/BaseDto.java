package br.com.example.abstracts;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDto implements Serializable {

    private Long id;
    private String nome;

    public BaseDto(BaseEntity entity) {
        this.id = entity.getId();
        this.nome = entity.getNome();
    }

}
