package br.com.example.abstracts;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    public abstract Long getId();
    public abstract void setId(Long id);

    @Column(name = "nome")
    private String nome;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_alteracao")
    private Date dataAlteracao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_insercao")
    private Date dataInsercao;

    @Column(name = "usuario_alteracao")
    private String usuarioAlteracao;

    @Column(name = "excluido")
    private Boolean excluido = Boolean.FALSE;

    @Column(name = "ativo")
    private Boolean ativo = Boolean.TRUE;


}
