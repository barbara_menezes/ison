package br.com.example.dto;

import br.com.example.abstracts.BaseDto;
import br.com.example.domain.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UsuarioDto extends BaseDto {

    private String login;
    private String senha;
    private String telefone;
    private String celular;

    public UsuarioDto(Usuario entity) {
        super(entity);
    }
    
    public UsuarioDto() {}
}
