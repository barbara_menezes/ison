package br.com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.example.abstracts.BaseRepository;
import br.com.example.abstracts.BaseService;
import br.com.example.domain.Usuario;
import br.com.example.repository.UsuarioRepository;

@Service
public class UsuarioService extends BaseService<Usuario> {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public BaseRepository<Usuario> getRepository() {
        return repository;
    }

}
