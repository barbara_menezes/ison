package br.com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.example.abstracts.BaseRestController;
import br.com.example.abstracts.BaseService;
import br.com.example.domain.Usuario;
import br.com.example.service.UsuarioService;

@RestController
@RequestMapping("usuario")
public class UsuarioController extends BaseRestController<Usuario> {

	@Autowired
    private UsuarioService service;

    @Override
    protected BaseService<Usuario> getService() {
        return service;
    }

}
